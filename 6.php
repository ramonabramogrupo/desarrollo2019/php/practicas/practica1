<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                border: 1px solid black;
                width:100px;
                height: 100px;
                margin: 20px auto;
                font-size: 4em;
                line-height: 100px;
                text-align: center;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <?php
        // tengo dos variables y quiero realizar 
        // una elevada a otra y mostrar el resultado y las dos 
        // variables
        ?>

        <?php

        /**
         * Funcion que calcula una potencia
         * @param numero $base 
         * @param numero $n 
         * @return numero
         */
        function potencia($base, $n) {
            // variables locales
            // $p y $i
            
            $p = 1;
            for ($i = 1; $i <= $n; $i++) {
                $p = $p * $base;
            }
            return $p;
        }

        // variables globales
        $b = 2;
        $e = 5;
        $r = potencia($b, $e);
        ?>
        
        <div>
            <?= $b ?>
        </div>
        
        <div>
            <?= $e ?>
        </div>
        
        <div>
            <?= $r ?>
        </div>
    </body>
</html>
